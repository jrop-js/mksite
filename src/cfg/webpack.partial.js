import base from './webpack.base'
import merge from 'webpack-merge'
import path from 'path'

export default config => {
	return merge(base(config), {
		target: 'node',
		output: {
			libraryTarget: 'commonjs',
			path: config.dirs.build.intermediate,
		},
		externals: (ctx, req, done) => {
			const ext = path.extname(req)
			const include =
				(ext != '' && ext != '.js') ||
				req.startsWith('/') ||
				req.startsWith('.')
			if (!include) return done(null, `commonjs ${req}`)
			return done()
		},
	})
}
