const LOADER_PREFIX = process.env.TEST == 'true' ? './lib' : require('../../package').name
export default config => ({
	entry: Object.assign({}, ...config.files.map(info => info.entry)),
	output: {
		filename: '[name].js',
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
			},
			{
				test: /\.md$/,
				use: [
					'babel-loader',
					{
						loader: `${LOADER_PREFIX}/md/loader`,
						options: config,
					},
				],
			},
			{
				test: req => req.includes(config.dirs.src) && req.endsWith('.js'),
				use: [
					'babel-loader',
					{
						loader: `${LOADER_PREFIX}/js/loader`,
						options: config,
					},
				],
			},
		],
	},
})
