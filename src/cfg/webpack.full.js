import base from './webpack.base'
import merge from 'webpack-merge'
import path from 'path'
import rename from '../rename-ext'
import webpack from 'webpack'

import DynamicCdnPlugin from 'dynamic-cdn-webpack-plugin'
import HtmlPlugin from 'html-webpack-plugin'
import UglifyJsPlugin from 'uglifyjs-webpack-plugin'

export default config =>
	merge(base(config), {
		devtool: 'source-map',
		output: {
			path: config.dirs.dist,
		},
		plugins: [
			new DynamicCdnPlugin({
				disable: process.env.NODE_ENV != 'production',
				only: ['react', 'react-dom', 'react-router-dom'],
			}),
			...config.files.map(
				file =>
					new HtmlPlugin({
						template: path.join(config.dirs.src, '_partials', 'index.html'),
						chunks: [rename(file.js, '')],
						filename: rename(file.js, '.html'),
						preRendered: file.html,
					})
			),
			...(process.env.NODE_ENV == 'production'
				? [
						new webpack.DefinePlugin({
							'process.env.NODE_ENV': JSON.stringify('production'),
						}),
						new UglifyJsPlugin({
							sourceMap: true,
							uglifyOptions: {
								output: {
									comments: false,
								},
							},
						}),
					]
				: []),
		],
	})
