import path from 'path'
export default (f, ext) =>
	path.format(Object.assign(path.parse(f), {ext, base: null}))
