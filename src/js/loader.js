import fmt, {lit} from './fmt'
import path from 'path'

export default function(source) {
	const config = this.query
	const suffix = fmt(
		`
			/***************/
			/** START APP **/
			/***************/
			import ReactDOM from 'react-dom'
			import Router from ${lit(path.resolve(config.dirs.build.gen, 'router'))}
			if (require.main === module && typeof window !== 'undefined') {
				ReactDOM.render(<Router />, document.getElementById('react-root'))
			}
		`
	)
	return `${source}\n${suffix}`
}
