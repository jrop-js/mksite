import * as babel from 'babel-core'

export default s =>
	babel.transform(s, {
		presets: ['react'],
		plugins: ['syntax-dynamic-import'],
	}).code

export const lit = x => JSON.stringify(x, null, 2)
