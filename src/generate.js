import fs from 'fs'
import mkdirp from 'mkdirp'
import Module from 'module'
import path from 'path'
import thunk from 'thunk-to-promise'

import React from 'react'
import ReactDOM from 'react-dom/server'
import {MemoryRouter} from 'react-router-dom'

import fmt from './js/fmt'

export default async function(config) {
	;(await Promise.all(
		config.files.map(async file => {
			const {js} = file
			const filename = path.join(config.dirs.build.intermediate, js)
			const contents = await thunk(done => fs.readFile(filename, 'utf8', done))
			delete require.cache[filename]
			const mod = new Module(filename, module)
			mod.filename = filename
			mod.paths = [path.join(process.cwd(), 'node_modules')]
			mod._compile(contents, filename)
			return Object.assign(file, {exports: mod.exports})
		})
	)).map(f => {
		const el = (
			<MemoryRouter>
				{React.createElement(f.exports.default || f.exports)}
			</MemoryRouter>
		)
		const html = ReactDOM.renderToStaticMarkup(el)
		return Object.assign(f, {html, props: el.props})
	})

	const router = fmt(
		`
			import React from 'react'
			import {BrowserRouter as Router, Route} from 'react-router-dom'

			const __lazy__ = modulePromise => class extends React.Component {
				constructor() {
					super()
					this.state = {component: 'Loading...'}
				}
				componentDidMount() {
					modulePromise.then(module =>
						this.setState({component: React.createElement(module.default || module)})
					)
				}
				render() {
					return this.state.component
				}
			}

			export default () => <Router>
				<div>
					${config.files
						.map(
							file =>
								`<Route path="${file.url}" exact component={() => React.createElement(__lazy__(import('${path.join(
									config.dirs.src,
									file.file
								)}')))} />`
						)
						.join('\n')}
				</div>
			</Router>
		`
	)

	await thunk(done => mkdirp(config.dirs.build.gen, done))
	await Promise.all([
		thunk(done =>
			fs.writeFile(path.join(config.dirs.build.gen, 'router.js'), router, done)
		),
		thunk(done =>
			fs.writeFile(
				path.join(config.dirs.build.gen, 'toc.json'),
				JSON.stringify(config.files, null, 2),
				done
			)
		),
	])
}
