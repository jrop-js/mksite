import globby from 'globby'
import path from 'path'
import rename from './rename-ext'
import thunk from 'thunk-to-promise'
import webpack from 'webpack'
import yargs from 'yargs'

import getPartialWebpackConfig from './cfg/webpack.partial'
import getFullWebpackConfig from './cfg/webpack.full'

import scaffold from './scaffold'
import generateTocAndRouter from './generate'

async function main() {
	const pkg = require('../package')
	const argv = yargs
		.usage('$0 [options]')
		.version('version', pkg.version)
		.alias('v', 'version')
		.help('help')
		.alias('h', 'help')
		.options({
			src: {
				alias: 's',
				discription: 'The source directory',
				type: 'string',
				default: 'src',
			},
			build: {
				alias: 'b',
				discription: 'The build directory',
				type: 'string',
				default: `node_modules/.${pkg.name}-cache`,
				hidden: true,
			},
			dest: {
				alias: 'd',
				discription: 'The distribution directory',
				type: 'string',
				default: 'dist',
			},
		}).argv

	const cwd = process.cwd()
	const dirs = {
		src: path.resolve(cwd, argv.src),
		build: {
			intermediate: path.resolve(cwd, argv.build, 'intermediate'),
			gen: path.resolve(cwd, argv.build, 'gen'),
		},
		dist: path.resolve(cwd, argv.dest),
	}
	const files = (await globby(
		[path.join(dirs.src, '**', '*.{js,jsx,md}'), '!**/_partials/**/*.*'],
		{
			absolute: true,
		}
	)).map(file => {
		const relative = path.relative(dirs.src, file)
		const js = rename(relative, '.js')
		const rawUrl = path.join('/', rename(relative, ''))
		return {
			file: relative,
			js,
			url: rawUrl == '/index' ? '/' : rawUrl,
			htmlFile:
				js == 'index.js'
					? 'index.html'
					: path.join(rename(relative, ''), 'index.html'),
			entry: {[rename(js, '')]: file},
		}
	})
	const config = {dirs, files}
	await pipeline(config)
}
main().catch(e => {
	process.exitCode = 1
	console.error(e)
})

async function pipeline(config) {
	await scaffold(config)

	const partialStats = await thunk(done =>
		webpack(getPartialWebpackConfig(config), done)
	)
	console.log(partialStats.toString({colors: true}))

	await generateTocAndRouter(config)

	const fullStats = await thunk(done =>
		webpack(getFullWebpackConfig(config), done)
	)
	console.log(fullStats.toString({colors: true}))
}
