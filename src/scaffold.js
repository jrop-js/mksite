import fmt from './js/fmt'
import fs from 'fs'
import mkdirp from 'mkdirp'
import path from 'path'
import thunk from 'thunk-to-promise'

export default async function(config) {
	const genDir = config.dirs.build.gen
	await thunk(done => mkdirp(genDir, done))

	await Promise.all([
		//
		// .babelrc
		//
		thunk(done =>
			fs.writeFile(
				path.join(genDir, '.babelrc'),
				JSON.stringify(
					{
						presets: ['react'],
						plugins: ['syntax-dynamic-import'],
					},
					null,
					2
				),
				done
			)
		),

		//
		// router.js
		//
		thunk(done =>
			fs.writeFile(
				path.join(genDir, 'router.js'),
				fmt(
					`
						import {MemoryRouter} from 'react-router-dom'
						export default () => <MemoryRouter />
					`
				),
				done
			)
		),

		//
		// toc.json
		//
		thunk(done => fs.writeFile(path.join(genDir, 'toc.json'), '{}', done)),
	])
}
