import fm from 'front-matter'
import marked from 'marked'
import path from 'path'

import fmt, {lit} from '../js/fmt'

export default function(source) {
	const config = this.query
	const {attributes, body} = fm(source)
	const html = marked(body)
	return fmt(
		`
			import React from 'react'
			import Layout from ${lit(
				path.join(config.dirs.src, '_partials', attributes.layout || 'index')
			)}
			const Markdown = props => <Layout {...props}>
				<div dangerouslySetInnerHTML={{__html: ${lit(html)}}} />
			</Layout>
			Markdown.defaultProps = ${lit(attributes)}
			export default Markdown
		`
	)
}
