# mksite

> Static-site generation how it was meant to be: React + Webpack = ❤️

## Use

1. `npm install --save-dev mksite`
2. `mksite`
3. ☕️

## Structure

```
package.json
src/
  _partials/
    index.js # layout
  index.js   # /index.html
  about.js   # /about.html
```

`mksite` generates a page per file (excluding the _partials/ directory).  It also extracts meta-data using React props.  If you want to expose metadata for your component, use `defaultProps`:

```js
// index.js
import React from 'react'
import Layout from './_partials'

const Index = props => (
	<Layout {...props}>
		My page!
	</Layout>
)
Index.defaultProps = {
	title: 'Title - Index',
}

export default Index
```

## Links

```js
import {Link} from 'react-router-dom'

export default () => <div>
	My page<br />
	<Link to="/other-page">Go to Other Page</Link>
</div>

// No 🚀-science: 🙌
```

## Advanced (kind-of)

```js
import tableOfContents from 'gen/toc'
doSomethingWith(tableOfContents)
// 😎
```

# License (ISC)

ISC License (ISC)
Copyright 2017 Jonathan Apodaca <jrapodaca@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
