import React from 'react'
import Layout from './_partials'

import {Link} from 'react-router-dom'

const Index = props => (
	<Layout {...props}>
		This is index<br />
		<Link to="/about">Go to About</Link>
	</Layout>
)
Index.defaultProps = {
	title: 'Index',
}
export default Index
