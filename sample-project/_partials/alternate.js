import React from 'react'
export default props => (
	<div>
		<h1>Alternate</h1>
		<h2>{props.title}</h2>
		{props.children}
	</div>
)
