import React from 'react'
import Layout from './_partials'

import {Link} from 'react-router-dom'

const About = props => (
	<Layout {...props}>
		This is about<br />
		<Link to="/">Go to Index</Link>
	</Layout>
)
About.defaultProps = {
	title: 'About',
}
export default About
